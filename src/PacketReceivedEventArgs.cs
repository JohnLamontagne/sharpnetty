﻿using System;

namespace SharpNetty
{
    public class PacketReceivedEventArgs : EventArgs
    {
        public Packet Packet { get; set; }

        public PacketReceivedEventArgs(Packet packet)
        {
            this.Packet = packet;
        }
    }
}