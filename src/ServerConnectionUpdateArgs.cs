﻿using System;

namespace SharpNetty
{
    public class ServerConnectionUpdateArgs : EventArgs
    {
        public int SocketIndex { get; set; }

        public ServerConnectionUpdateArgs(int socketIndex)
        {
            this.SocketIndex = socketIndex;
        }
    }
}